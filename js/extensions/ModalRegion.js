define(['marionette', 'bootstrap'], 
        function(marionette, bootstrap) {
  'use strict'

  return marionette.Region.extend({
    el: '#modal',

    constructor: function(){
      _.bindAll(this);
      marionette.Region.prototype.constructor.apply(this, arguments);
      this.on('view:show', this.showModal, this);
    },

    getEl: function(selector){
      var $el = $(selector);
      $el.on('hidden', this.close);
      return $el;
    },

    showModal: function(view){
      if (view.options.swap != true) {
        view.on('close', this.hideModal, this);
      };
      this.$el.modal('show');
    },

    hideModal: function(){
      //-- we want to suppress this when swap === true
      this.$el.modal('hide');
    },

  });
});

